#pragma once

#include <stdbool.h>

/** A circular double linked list which at the same time is a node in that list itself */
struct list
{
	struct list *next, *prev;
};

/** Initializes an empty list (this will not remove old nodes if any exist) */
static inline void list_init(struct list *const list)
{
	list->next = list->prev = list;
}

/** Returns `true` when the list is empty */
static inline bool list_empty(struct list const *const list)
{
	return list->next == list;
}

#define list_foreach(head, iter) \
	for(struct list *next_, *iter = (head)->next; next_ = iter->next, iter != (head); iter = next_)

#define container_of(ptr, type, member) \
	((type *)(((char *)ptr) - offsetof(type, member)))

/** Appends `elem` at the end of `list` */
static inline void list_append(
		struct list *const list,
		struct list *const elem)
{
	struct list *const end = list->prev;

	elem->next = list;
	end->next = elem;
	elem->prev = end;
	list->prev = elem;
}

/** Safely removes `elem` from it's list while leaving the circle closed */
static inline struct list *list_unlink(
		struct list *const elem)
{
	struct list *const next = elem->next;

	elem->next->prev = elem->prev;
	elem->prev->next = elem->next;

	return next;
}

#define list_destroy(head, type, member) \
	do \
	{ \
		list_foreach(head, elem_) \
		{ \
			list_unlink(elem_); \
			free(container_of(elem_, type, member)); \
		} \
	} while(0)
