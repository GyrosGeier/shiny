#include "x11_display.h"

#include "global.h"

#include <stdlib.h>

bool x11_display_setup(struct global *const g)
{
	Display *const dpy = XOpenDisplay(NULL);
	if(!dpy)
		goto fail_display;

	g->x11.dpy = dpy;

	g->x11.self = XInternAtom(dpy, "shiny", False);

	if(list_empty(&g->x11.managed_roots))
	{
		int const num_screens = ScreenCount(dpy);

		for(int screen = 0; screen < num_screens; ++screen)
		{
			//Screen *scr = ScreenOfDisplay(dpy, screen);
			Window root = RootWindow(dpy, screen);
			struct x11_managed_root *const new_root = malloc(sizeof *new_root);
			if(!new_root)
				goto fail_alloc;

			new_root->id = root;

			list_append(&g->x11.managed_roots, &new_root->siblings);
		}
	}

	list_foreach(&g->x11.managed_roots, node)
	{
		struct x11_managed_root *const root =
				container_of(node, struct x11_managed_root, siblings);

		XSelectInput(dpy, root->id, SubstructureRedirectMask|PropertyChangeMask);
	}

	return true;

fail_alloc:
	XCloseDisplay(dpy);
	g->x11.dpy = NULL;

fail_display:
	return false;
}

void x11_display_teardown(struct global *const g)
{
	if(g->x11.dpy)
		XCloseDisplay(g->x11.dpy);
	g->x11.dpy = NULL;

	return;
}
